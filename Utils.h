#include <iostream>
#include <vector>

#ifndef PATTERNRECOGNITION_UTILS_H
#define PATTERNRECOGNITION_UTILS_H

using std::vector;


class Utils {
public:
    void prtMap(vector<vector<int> > &map, bool indent);
    void prtLine(int len);
};


#endif //PATTERNRECOGNITION_UTILS_H
