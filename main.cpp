#include <iostream>
#include <vector>
#include "Utils.h"

using namespace std;

/*
 * ALERT: This code is horribly old could highly improved
 */

/**
 * A simple Pattern Recognition Algorithm by Max Schiller
 * contact: schiller.maximilian@gmail.com
 *
 * Copyright (c) Munich, 15.11.2016 by Max Schiller -  all rights reserved
 *
 *
 * Cell Convention: [Y][X]
 *
 * The following code is crappy and NOT cleaned up.
 * TODO: Refactor Code in future!
 */


vector<vector<int> > map;

/**
 * Recursive Search is deprecated cause of lousy efficiency (and not fully working)
 */
int testHorz(int y, int x, int depthX){
    if (depthX >= 0){
        if (map[y][x] == map[y][x + depthX]){
            //TODO: Set -1 to a maximum
            depthX--;

            return testHorz(y, x, depthX);
        } else {
            return depthX;
        }
    } else {
        return depthX;
    }
}


/**
 * Recursive Search is deprecated. See Above
 */
int expandVert(int y, int x, int depthY, int depthX){
    int count = testHorz(y, x, depthX);
    if (count == depthX){
        //TODO set to a maximum
        return expandVert(y, x, depthY + 1, depthX);
    } else {
        return depthY;
    }
}

/**
 * Shit.
 */
std::pair<int, int> recognize(int y, int x, int offsetY, int offsetX, int localY, int localX){

    if (map[localY][localX] == map[y + (y - localY)][x + (x - localX)]){
        localX++;
        if (localX > x){
            localY++;
            localX = offsetX;
        }
        if (localY > y){
            return std::make_pair(x - offsetX, y - offsetY);
        }

        recognize(y, x, offsetX, offsetY, localY, localX);

    } else {
        return std::make_pair(1,1);
    }

}




/**
 * @return relative Position on Pattern
 * NOT IN USE
 */
double getPatternPos(int pos, int length){

    return ((pos / length) - (int)length) * length;;

}


/**
 * y,x is LEFT UPPER Corner
 * @return vector<vector<int> > of Pattern with give size on give position.
 */
vector<vector<int> > getPattern(int y, int x, int height, int width, vector<vector<int> > &map){
    vector<vector<int> > pattern;
    pattern.resize(height);

    for (int innerY = 0; innerY < height; ++innerY) {
        pattern[innerY].resize(width);
        for (int innerX = 0; innerX < width; ++innerX) {
            pattern[innerY][innerX] = map[y + innerY][x + innerX];
        }
    }

    return pattern;
}

/**
 * Try mapping the given Pattern on map
 * posY,posX is LEFT UPPER Corner
 *
 * @return pair<int,int> of matches and mismatches
 */
pair<int,int> testSurrounding(int posY, int posX, vector<vector<int> > &map, vector<vector<int> > &pattern){

    //TODO: Notice to test Edges if posY or posX != 0 (truncated Patterns)

    int patternWidth = (int)pattern[0].size();
    int patternHeight = (int)pattern.size();
    int width = (int)map[0].size();
    int height = (int)map.size();
    int globalCount = 0;
    int count = 0;
    int match = 0;
    int mismatch = 0;

    for (int y = 0; y < height; y += patternHeight) {
        for (int x = 0; x < width; x += patternWidth) {


            //Iterate threw Map and on each position
            for (int innerY = 0; innerY < patternHeight; ++innerY) {
                for (int innerX = 0; innerX < patternWidth; ++innerX) {
                    //if map + pos ==
                    //Mapping Position after iteration
                    //TODO: Check me later: OK, I think
                    //TODO: Add overflow prevention

                    if (!(y + posY >= height || x + posX >= width)) {

                        //cout << map[y + posY + innerY][x + posX + innerX] << " == " << pattern[innerY][innerX] << " " << y + posY + innerY << ":" << x + posX + innerX << endl;

                        if (map[y + posY + innerY][x + posX + innerX] == pattern[innerY][innerX]) {
                            //count++;
                            match++;
                            //cout << " " << count << ":" << globalCount << endl;
                        } else {
                            mismatch++;
                            //cout << endl;
                            //continue;
                        }
                    }

                }
            }

            /*if (count == patternWidth * patternHeight){
                globalCount++;
                count = 0;
            }*/

            //TODO: Apply this to matches and mismatches
            //Miss Match was here




            /* Iterate threw whole map (!x++ & y++) with jumps one pattern
             * Senseless cause there is no way to count fully matching patterns

            //get Position in Pattern to iterate threw pattern multiple times
            double patternPosX = getPatternPos(x, patternWidth);
            double patternPosY = getPatternPos(y, patternHeight);

            if (pattern[(int)patternPosY][(int)patternPosX] == map[y + posY][x + posX]){

                //count++;

            }*/


        }
    }

    //TODO: return Matches count

    return make_pair(match, mismatch);
}



/**
 * Rectangular Search
 * Working with Offset to archive rectangular Shape instead of searching in squares
 */
std::pair<int,int> compareHorizontal(int y, int x, int offset){

    int distance = x - offset;
    int height = 0;

    int width = 0;

    //TODO: Refactor whole Structure

    if (distance > 0) {
        for (int innerY = y; innerY < y + distance; ++innerY) {
            for (int innerX = offset; innerX < x; ++innerX) {
                if (map[innerY][innerX] == map[innerY][innerX + distance]) {
                    //For Debugging
                    //cout << map[innerY][innerX] << " == " << map[innerY][innerX + distance] << " true" << endl;
                    //cout << innerY << "," << innerX << " == " << innerY << "," << innerX + distance << " true" << endl;
                    width++;
                } else {
                    //for Debugging
                    //cout << map[innerY][innerX] << " == " << map[innerY][innerX + distance] << " false" << endl;
                    //cout << innerY << "," << innerX << " == " << innerY << "," << innerX + distance << " false" << endl;
                    return make_pair(height, width);
                }

            }

            if (width == distance) {
                height++;

                if (height == distance){
                    return make_pair(height, width);
                }
                width = 0;
            } else {
                return make_pair(height, width);
            }
        }
    }
}

/**
 * Triangularic Field Search Algorithm (TF-SA) - DEPRECATED
 * Compares two Sides  (x - distance with x) with an triangularic Field for asymetric Patterns but not in use
 *
 * Example:
 *    Compare # with X (# == X)
 *
 *    #  |X
 *    #  |X
 *    #  |X
 *
 *    ## |XX
 *    ## |XX
 *
 *    ###|XXX
 *
 *    x++ and repeat
 *
 * Offset is the Start Point
 *
 * @return: vector<int> pattern;
 * 0 = y, 1 = x, 2 = height, 3 = width
 */

vector<int> compareTriangular(vector<vector<int> > &map){
    int HEIGHT = (int)map.size();
    int WIDTH = (int)map[0].size();

    for (int y = 0; y < HEIGHT; ++y) {
        for (int x = 0; x < WIDTH; ++x) {

            for (int offset = 0; offset < x; ++offset) {
                
                int distance = x - offset;
                int height = distance;
                int width = 1;
                int heightSum = 0;
                int widthSum = 0;


                for (int field = 0; field < distance; ++field) {

                    //Calc Triangle and Mapping it on map
                    for (int innerY = 0; innerY < height; ++innerY) {
                        for (int innerX = 0; innerX < width; ++innerX) {

                            if (map[y + innerY][offset + innerX] == map[y + innerY][x + innerX]){
                                widthSum++;
                            } else {
                                continue;
                            }

                        }

                        if (widthSum == width){
                            heightSum++;
                            widthSum = 0;

                        } else {
                            //do nothing because no result for this Field
                        }
                    }


                    if (heightSum == height){

                        //testSurrounding(y, x, );


                        //TODO: Add return here
                    }

                }

                
            }
            
            
        }
    }
}



int main() {

    vector<vector<int> > pattern;
    Utils utils;

    pattern = {

        {1,0,1},
        {0,1,0},
        {1,0,1}

    };

    /*
     * Setting up a Test map
     */


    map.resize(12);
    for (int i = 0; i < map.size(); ++i) {
        map[i].resize(11);
    }

    int height = (int)pattern.size();
    int width = (int)pattern[0].size();

    //Mapping pattern to map and rescale
    for (int y = 0; y < map.size(); y = y + height) {
        for (int x = 0; x < map[0].size(); x = x + width) {
            for (int yin = 0; yin < height; ++yin) {
                for (int xin = 0; xin < width; ++xin) {
                    if (y + yin < map.size() || x + xin < map[0].size()){
                        map[y + yin][x + xin] = pattern[yin][xin];
                    }

                }
            }
        }
    }

    utils.prtMap(map, false);
    utils.prtLine(20);

    /*
     * Map set up
     * --------------------------------------------------------------------------------------
     * Pattern Recognition
     */


    for (int y = 0; y < map.size(); ++y) {
        for (int x = 0; x < map[0].size(); ++x) {

            for (int offset = 0; offset < x; ++offset) {
                cout << y << "," << x << ";" << offset << endl;
                pair<int,int> size = compareHorizontal(y, x, offset);

                if (size.first > 1 && size.second > 1){
                    vector<vector<int> > localPattern = getPattern(y, x, size.second, size.first, map);
                    utils.prtMap(localPattern, true);
                    pair<int,int> matches = testSurrounding(y, x - size.second, map, localPattern);
                    cout << "matches: " << matches.first << endl;
                    cout << "mismatches: " << matches.second << endl;
                }

            }

        }
    }

    //compareTriangular(map);



    return 0;
}



