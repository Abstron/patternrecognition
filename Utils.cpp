#include <iostream>
#include <vector>
#include "Utils.h"

using namespace std;
using std::vector;

void Utils::prtMap(vector<vector<int> > &map, bool indent){
    for (int y = 0; y < map.size(); ++y) {

        if (indent){
            cout << "   ";
        }

        for (int x = 0; x < map[0].size(); ++x) {
            cout << map[y][x];
        }
        cout << endl;
    }
}

void Utils::prtLine(int len) {
    for (int i = 0; i < len; ++i) {
        cout << "-";
    }
    cout << endl;
}